# Solidity 
## Basic steps to go

> We should init a contract writing:

`pragma solidity ^0.4.0`

We have to write the solidity version to avoid problems with versions.

1. We should create the contract typing "Contract"

```
contract ContractName {}
```

**Hello World Example:**

```
string word = "Hello World";
```

Now, we create the getter function to return the "word" value;

```
function getWord() constant returns(string){
    return word;
}
```
Now, we can create our setter function in order to change our "word" value.
```
function setWord(string newWord) returns(string) {
    word = newWord;
    return word;
}
```
Setter function doesn't need the constant word.

If we want to protect the contract, we need the owner's address, so we can create the variable "owner" which is type address.

```
address owner;
```
Inside the setter function we can create a conditional and we need the reserved word "msg.sender". msg.sender has the address where the contract is called.

```
function setWord(string newWord) returns(string) {
    if(owner != msg.sender){
        return "you are not the contract owner";
    }
    word = newWord;
    return "You are the contract owner";
}
```

Create the constructor
```
function ContractName() {
    owner = msg.sender;
}
```
The constructor is execute just one time, when contract is create. Entonces cuando creamos el contrato la primera vez, la variable owner va a tomar la dirección de quien envía el contrato y entonces solo esa persona puede usar la function setWord por ejemplo.

**Another way to control who could modify a variable in the contract is using modifiers**

**Modifiers are a professional way to control that**

**After the constructor function, we have to create the modifiers**

```
modifier modifierName {
    if(owner != msg.sender) {
        revert();
    } else {
        _;
    }
}
```

**Now we have to modify the setter function, we should add the modifier, like this:**

```
function setWord(string newWord) modifierName returns(string) {
    word = newWord;
    return "You successfully changed the variable word";
}
```

Check that we added the modifier just before the `returns(string)`

**REMEMBER THE CONTRACT CONSTRUCTOR IS CALLED JUST ONE TIME**

We can init a contract with some parameters in the constructor:

```
function ContractTest(string _word) {
    word = _word;
    number = 42;
    owner = msg.sender;
}
```

We can create events that could be call when something happened, for example the next event will be call when something in the contract was changed, is like a log.

```
event Changed(string msg, address user, uint256 time);
```

And we can call the event anywhere we want to save a change, for example:

```
function setWord(string w) onlyOwner {
    word = w;
    Changed(msg.sender);
}
```

Another way to use a modifier:

```
modifier onlyOwner {
    require(msg.sender == owner);
    _;
}
```

Reserved Word "Payable"
It let function can receive cash

Add payable to the function in order to receive cash/funds/money.

```
function receiveFunds() payable {

}
```

To get the contract balance we could create the next function:

```
function getBalance() constant returns(uint){
    return this.balance;
}
```

Communication between contracts

We have to create a contract and in the constructor we can call the other contract:
```
contract CallerContract {
    NameContract toBeCalled = new NameContract();
}
```

Herencias:
Inheritance:

Just adding "is ContractName" we create an inheritance inside another contract.

```
contract User is Mortal {
    ......
}
```

Array in solidity:

```
type[] nameVariable = [algo, algo, algo];
```
```
address[] employees = [0x14723a09acff6d2a60dcdf7aa4aff308fddc160c, 0x4b0897b0513fdc7c541b6d9d7e929c4e5364d2db, 0x4b0897b0513fdc7c541b6d9d7e929c4e5364d2db];
```

If we use "internal" in a function it will work just inside the contract, we can't use this function outside the contract:

```
function updateTotal() internal {
    totalRecieved += msg.value;
}
```

struct es para crear un objeto.

```
struct Person {
    //attrs
    string name;
    uint age;
    bool active;
}
```
Solidity no tiene función para números random, entonces se puede usar el tiempo, para mostrar el tiempo se usa:

```
block.timestamp
```

block.timestamp se maneja por segundos, por ejemplo

```
lastTime = block.timestamp;
if(block.timestamp > (lastTime + 120)) {
    do something ...
}
```
acá se preguntó si ya ha pasado más de 2 minutos entonces que haga algo.
*********************************

Para revisar la plata del contrato se usa balance.

```
this.balance
```

Para enviarle dinero a alguien que ejecutó una función en el contrato se usa:

```
msg.sender.transfer(amount);
```
con "transfer" se transfiere dinero.

