pragma solidity ^0.4.11;

contract Tokens {

    address owner;
    address[] public votersAddress;

    event Status(string msg, address user, uint256 time);    

    function Tokens() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        if (owner != msg.sender) {
            revert();
        } else {
            _;
        }
    }

    function addAddressVoters(address _votersAddress) onlyOwner {
        Status("New voter added", _votersAddress, block.timestamp);
        votersAddress.push(_votersAddress);
    }

    function addressFind(address _address) returns(bool) {
        bool found;
        found = false;
        for (uint index = 0; index < votersAddress.length; index++ ) {
            if (votersAddress[index] == _address) {
                index == votersAddress.length;
                found = true;
            }
        }
        return found;
    }

}