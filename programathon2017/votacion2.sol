pragma solidity ^0.4.11;

contract BallotProgramathon {
    address owner;
    uint ballotEnd;
    uint ballotStart;
    bool progress;
    string question;

    event StatusVote(string msg, address user, uint time);

    struct Voter {
        bytes32 voterEmail;
        bool voted;
        bytes32 vote;
        bytes32[] voteMultiple;
        uint token;
    }

    struct Proposal {
        bytes32 name;
        uint voteCount;
    }

    modifier onlyOwner() {
        if (owner != msg.sender) {
            revert();
        } else {
            _;
        }
    }

    mapping(uint => Voter) public voterList;
    mapping(bytes32 => Proposal) public proposalList;

    Voter[] public _voters;
    Proposal[] public proposals;

    function BallotProgramathon() {
        owner = msg.sender;
        StatusVote("contract init", owner, block.timestamp);
    }

    function getVotersToken() returns (bytes32[], uint[]) {
        bytes32[] memory emailList = new bytes32[](_voters.length);
        uint[] memory tokenList = new uint[](_voters.length);

        for (uint index = 0; index < _voters.length; index++) {
            emailList[index] = _voters[index].voterEmail;
            tokenList[index] = _voters[index].token;
        }

        return (emailList, tokenList);
    }

    // add voters
    function setVoters(bytes32[] _emailList) {     
        for (uint index = 0; index < _emailList.length; index++) {  
            uint token = block.timestamp + index % _emailList.length;

            voterList[token] = Voter({
                voterEmail: _emailList[index],
                voted: false,
                vote: "",
                voteMultiple : new bytes32[](0),
                token: token
            });
            _voters.push(voterList[token]);
        }
        StatusVote("new VOTER added", msg.sender, block.timestamp);
    }

    function vote(uint _token, bytes32 optionSelected, uint voteType) {
        require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp);
        Voter storage currentVoter = voterList[_token];  
        Proposal storage currentProposal = proposalList[optionSelected];         
         
        if (!currentVoter.voted) {
            currentVoter.voted = true;
            currentProposal.voteCount += 1;
            //0 = public && 1 = private
            if ( voteType == 0 ) {
                currentVoter.vote = currentProposal.name;
                currentVoter.voteMultiple = new bytes32[](0);
            }
            StatusVote("new vote added", msg.sender, block.timestamp);
        }else {
            StatusVote("this address has a vote", msg.sender, block.timestamp);
        }   
    }

    function voteMultiple(uint _token, bytes32[] optionSelected, uint voteType) {
        require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp); 
        Voter storage currentVoter = voterList[_token];        
        require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp); 

        Proposal storage currentProposal;
        
        if (!currentVoter.voted) {
            currentVoter.voted = true;

            for (uint itemIndex = 0; itemIndex < optionSelected.length; itemIndex++) {
                currentProposal = proposalList[optionSelected[itemIndex]];                
                currentProposal.voteCount += 1;
            }
            
            //0 = public && 1 = private
            if ( voteType == 0 ) {
                currentVoter.vote = "";
                currentVoter.voteMultiple = optionSelected;
            }
            StatusVote("new vote added", msg.sender, block.timestamp);
        }else {
            StatusVote("this address has a vote", msg.sender, block.timestamp);
        }       
        
    }

    function getMultipleVotes(uint _token) returns (bytes32[]) {
        bytes32[] memory na;

        Voter storage currentVoter = voterList[_token]; 
        na = new bytes32[](currentVoter.voteMultiple.length);
        
        for (uint v = 0; v < currentVoter.voteMultiple.length; v++) {
            na[v] = currentVoter.voteMultiple[v];
        }
        
        return na;
    }

    function fulfillProposal(bytes32[] proposalOptions) internal {
        require(block.timestamp < ballotStart);

        for (uint index = 0; index < proposalOptions.length; index++) {
            proposalList[proposalOptions[index]] = Proposal({
                name: proposalOptions[index],
                voteCount: 0
            }); 
            proposals.push(proposalList[proposalOptions[index]]);
        }
    }    

    // actualiza variables de votación mientras no se haya iniciado
    function addProposal(bytes32[] proposalOptions) onlyOwner {
        require(ballotStart > block.timestamp);
        fulfillProposal(proposalOptions);
        StatusVote("proposal was updated", msg.sender, block.timestamp);
    }

    // obtener progreso de votación
    function getPartialBallotResults() returns(bytes32[], uint[]) {
        bytes32[] memory proposalNames = new bytes32[](proposals.length);
        uint[] memory voteCounts = new uint[](proposals.length);

        if (progress) {
            for (uint index = 0; index < proposals.length; index++) {
                proposalNames[index] = proposals[index].name;
                voteCounts[index] = proposals[index].voteCount;
            }
        }
        StatusVote("return progress", msg.sender, block.timestamp);
        return (proposalNames, voteCounts);        
    }

    // obtener pregunta
    function getQuestion() returns(string) {
        return question;
    }

    // obtener fechas de la campania
    function getContractTime() returns(uint256, uint256) {
        return (ballotStart, ballotEnd);
    }

    // set pregunta
    function setQuestion(string _question) onlyOwner {
        require(ballotStart > block.timestamp);
        question = _question;
        StatusVote("question updated", msg.sender, block.timestamp);
    }

    // set progress
    function setProgress(bool _progress) onlyOwner {
        require(ballotStart > block.timestamp);
        progress = _progress;
        StatusVote("progress updated", msg.sender, block.timestamp);
    }

    // valor de la variable progress
    function getProgress() returns (bool) {
        return progress;
    }

    // actualizar fechas
    function setContractTime(uint256 _ballotStart, uint256 _ballotEnd) onlyOwner payable {
        require(_ballotStart > block.timestamp);
        require(_ballotStart < _ballotEnd);
        ballotStart = _ballotStart;
        ballotEnd = _ballotEnd;
    }

    // obtener proposals
    function getAllProposals() returns(bytes32[]) {
        bytes32[] memory proposalNames = new bytes32[](proposals.length);
        for (uint index = 0; index < proposals.length; index++) {
            proposalNames[index] = proposals[index].name;
        }
        return proposalNames;
    }

    function updateProposal(bytes32 oldValue, bytes32 newValue) onlyOwner returns(bool) {
        require(ballotStart > block.timestamp);
        Proposal storage currentProposal = proposalList[oldValue];
        currentProposal.name = newValue;
        StatusVote("updateProposal updated", msg.sender, block.timestamp);

        return true;
    }
}