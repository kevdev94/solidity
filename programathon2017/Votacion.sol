pragma solidity ^0.4.11;

contract BallotProgramathon {
    address owner;
    uint256 ballotEnd;
    uint256 ballotStart;
    bool progress;
    string question;

    event StatusVote(string msg, address user, uint256 time);

    struct Voter {
        bytes32 voterEmail;
        bool voted;
        bytes32 vote;
        bytes32[] voteMultiple;
        bytes32 token;
    }

    struct Proposal {
        bytes32 name;
        uint voteCount;
    }

    modifier onlyOwner() {
        if (owner != msg.sender) {
            revert();
        } else {
            _;
        }
    }

    mapping(bytes32 => Voter) public voterList;

    Voter[] public _voters;
    Proposal[] public proposals;

    function BallotProgramathon() {
        owner = msg.sender;
        StatusVote("contract init", owner, block.timestamp);
    }

    function getVotersToken() returns (bytes32[], bytes32[]) {
        bytes32[] memory emailList = new bytes32[](_voters.length);
        bytes32[] memory tokenList = new bytes32[](_voters.length);

        for (uint index = 0; index < _voters.length; index++) {
            emailList[index] = _voters[index].voterEmail;
            tokenList[index] = _voters[index].token;
        }

        return (emailList, tokenList);
    }

    // add voters
    function setVoters(bytes32[] _emailList) {     
        for (uint index = 0; index < _emailList.length; index++) {
            var (found, id) = emailFinder(_emailList[index]);
            id = 0;
            if (!found) {
                _voters.push(Voter({
                    voterEmail: _emailList[index],
                    voted: false,
                    vote: "",
                    voteMultiple : new bytes32[](0),
                    token: sha3(_emailList[index], block.timestamp)
                }));
                
            }
        }
        StatusVote("new VOTER added", msg.sender, block.timestamp);
    }

    function vote(bytes32 _token, uint optionSelected, uint voteType) {
        var (found, index) = tokenFinder(_token);
        if (found) {
            require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp);            
         
            if (!_voters[index].voted) {
                _voters[index].voted = true;
                proposals[optionSelected].voteCount += 1;
                //0 = public && 1 = private
                if ( voteType == 0 ) {
                    _voters[index].vote = proposals[optionSelected].name;
                    _voters[index].voteMultiple = new bytes32[](0);
                }
                StatusVote("new vote added", msg.sender, block.timestamp);
            }else {
                StatusVote("this address has a vote", msg.sender, block.timestamp);
            }       
        }
    }

    function voteMultiple(bytes32 _token, bytes32[] optionSelected, uint voteType) {
        
        var (found, index) = tokenFinder(_token);
        if (found) {
            require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp);            
         
            if (!_voters[index].voted) {
                _voters[index].voted = true;

                for (uint itemIndex = 0; itemIndex < optionSelected.length; itemIndex++) {
                    var (exist, position) = proposalFinder(optionSelected[itemIndex]);
                    exist;
                    proposals[position].voteCount += 1;
                }
                
                //0 = public && 1 = private
                if ( voteType == 0 ) {
                    _voters[index].vote = "";
                    _voters[index].voteMultiple = optionSelected;
                }
                StatusVote("new vote added", msg.sender, block.timestamp);
            }else {
                StatusVote("this address has a vote", msg.sender, block.timestamp);
            }       
        }
    }

    function getMultipleVotes(bytes32 _token) returns (bytes32[]) {
        bytes32[] memory na;
        for (uint index = 0; index < _voters.length; index++ ) {
            if (_voters[index].token == _token) {
                na = new bytes32[](_voters[index].voteMultiple.length);
                
                for (uint v = 0; v < _voters[index].voteMultiple.length; v++) {
                    na[v] = _voters[index].voteMultiple[v];
                }
                
                index == _voters.length;
            }
        }
        return na;
    }

    function fulfillProposal(bytes32[] proposalOptions) internal {
        //require(block.timestamp < ballotStart);
        for (uint index = 0; index < proposalOptions.length; index++) {
            var (found, position) = proposalFinder(proposalOptions[index]);
            position = 0;
            if (!found) {
                proposals.push(Proposal({
                    name: proposalOptions[index],
                    voteCount: 0
                }));
            } else {
                StatusVote("proposal already exists", msg.sender, block.timestamp);
            }
        }
    }

    // busca si existe la opcion
    function proposalFinder(bytes32 proposalName) returns(bool, uint) {
        bool found = false;
        uint position;
        for (uint index = 0; index < proposals.length; index++ ) {
            if (proposals[index].name == proposalName) {
                position = index;
                index == _voters.length;
                found = true;
            }
        }
        return (found, position);        
    }
    
    // busca si existe la dirección
    function emailFinder(bytes32 _email) returns(bool, uint) {
        bool found = false;
        uint position;
        for (uint index = 0; index < _voters.length; index++ ) {
            if (_voters[index].voterEmail == _email) {
                position = index;
                index == _voters.length;
                found = true;
            }
        }
        StatusVote("existe dirección", msg.sender, block.timestamp);
        return (found, position);        
    }

    // busca si el token existe
    function tokenFinder(bytes32 _token) returns(bool, uint) {
        bool found = false;
        uint position;
        for (uint index = 0; index < _voters.length; index++ ) {
            if (_voters[index].token == _token) {
                position = index;
                index == _voters.length;
                found = true;
            }
        }
        StatusVote("existe token", msg.sender, block.timestamp);
        return (found, position);        
    }

    // actualiza variables de votación mientras no se haya iniciado
    function addProposal(bytes32[] proposalOptions) onlyOwner {
        require(ballotStart <= block.timestamp && ballotEnd >= block.timestamp);
        fulfillProposal(proposalOptions);
        StatusVote("proposal was updated", msg.sender, block.timestamp);
    }

    // obtener progreso de votación
    function getPartialBallotResults() returns(bytes32[], uint[]) {
        bytes32[] memory proposalNames = new bytes32[](proposals.length);
        uint[] memory voteCounts = new uint[](proposals.length);

        if (progress) {
            for (uint index = 0; index < proposals.length; index++) {
                proposalNames[index] = proposals[index].name;
                voteCounts[index] = proposals[index].voteCount;
            }
        }
        StatusVote("return progress", msg.sender, block.timestamp);
        return (proposalNames, voteCounts);        
    }

    // obtener pregunta
    function getQuestion() returns(string) {
        return question;
    }

    // obtener fechas de la campania
    function getContractTime() returns(uint256, uint256) {
        return (ballotStart, ballotEnd);
    }

    // set pregunta
    function setQuestion(string _question) onlyOwner {
        require(ballotStart > block.timestamp);
        question = _question;
        StatusVote("question updated", msg.sender, block.timestamp);
    }

    // set progress
    function setProgress(bool _progress) onlyOwner {
        require(ballotStart > block.timestamp);
        progress = _progress;
        StatusVote("progress updated", msg.sender, block.timestamp);
    }

    // valor de la variable progress
    function getProgress() returns (bool) {
        return progress;
    }

    // actualizar fechas
    function setContractTime(uint256 _ballotStart, uint256 _ballotEnd) onlyOwner payable {
        require(_ballotStart > block.timestamp);
        require(_ballotStart < _ballotEnd);
        ballotStart = _ballotStart;
        ballotEnd = _ballotEnd;
    }

    // obtener proposals
    function getAllProposals() returns(bytes32[]) {
        bytes32[] memory proposalNames = new bytes32[](proposals.length);
        for (uint index = 0; index < proposals.length; index++) {
            proposalNames[index] = proposals[index].name;
        }
        return proposalNames;
    }

    function updateProposal(bytes32 oldValue, bytes32 newValue) onlyOwner returns(bool) {
        bool updated = false;
        require(ballotStart > block.timestamp);
        var (found, index) = proposalFinder(oldValue);
        if (found) {
            proposals[index].name = newValue;
            StatusVote("updateProposal updated", msg.sender, block.timestamp);
        } else {
            StatusVote("updateProposal not updated", msg.sender, block.timestamp);
        }
        
        return updated;
    }
}