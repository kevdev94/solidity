pragma solidity ^0.4.0;

contract Savings {
    address owner;

    event UpdateStatus(string msg);
    event UserStatus(string _msg, address user, uint amount);

    function Savings() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        if(owner != msg.sender){
            revert();
        } else {
            _;
        }
    }

    function kill() onlyOnwer {
        suicide(owner);
    }

    // depositar fondos
    function depositFunds(uint amount) onlyOwner {
        if(owner.send(amount)) {
            UserStatus('user depositó some money', msg.sender, msg.value);
        }
    }

    // sacar plata
    function withdrawnFunds(uint amount) onlyOwner {
        if(owner.send(amount)){
            UpdateStatus('user retiró plata');
        }
    }

    // para saber cuanto dinero hay
    function getFunds() onlyOwner constant returns(uint) {
        return this.balance;
    }

}