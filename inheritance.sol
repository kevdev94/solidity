pragma solidity ^0.4.0;

contract Mortal {
    address public owner;
    
    function Mortal() {
        owner = msg.sender;
    }
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
    
    function kill() onlyOwner {
        suicide(owner);
    }
}

contract User is Mortal {
    string public userName;
    
    function User(string _name) {
        userName = _name;
    }
}

contract Provider is Mortal {
    string public providerName;
    
    function Provider(string _name) {
        providerName = _name;
    }
}
