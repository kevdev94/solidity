pragma solidity ^0.4.0;

contract Tokens {

    address owner;
    uint256[] public tokens;
    
    mapping (address => uint256) public balanceOf;

    function Tokens() {
        owner = msg.sender;
    }

    function generateTokens(uint256 initialSupply) {

        balanceOf[owner] = initialSupply;
    }


    modifier onlyOwner() {
        if(owner != msg.sender){
            revert();
        } else {
            _;
        }
    }

}