pragma solidity ^0.4.0;

contract Seminar {
    struct Person {
        string name;
        uint age;
        bool active;
    }

    uint fee;
    uint loss = 80;

    mapping(address=>Person) public attendants;

    // PAYABLE PORQUE RECIBE DINERO
    function register(string _name, uint _age) payable {
        if(msg.value == fee){
            attendants[msg.sender] = Person({
                name: _name,
                age: _age,
                active: true
            });
        } else {
            revert();
        }
    }

    function setRegistration(uint256 _fee) {
        fee = _fee;
    }

    function cancelRegistration() {
        attendants[msg.sender].active = false;
        msg.sender.transfer((fee*loss)/100);
    }

}