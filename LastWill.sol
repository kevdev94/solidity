pragma solidity ^0.4.0;

contract LastWill {
    
    address owner;
    uint256 public lastTouch;
    address[] public childs;
    
    event Status(string msg, address user, uint256 time);
    
    modifier onlyOwner() {
        if(owner != msg.sender){
            revert();
        } else {
            _;
        }
    }
    
    function LastWill() payable {
        owner = msg.sender;
        lastTouch = block.timestamp;
        Status("LastWill contract was created", msg.sender, block.timestamp);
    }
    
    // con solo tener payable ya puede recibir dinero la función
    function depositFunds() payable {
        Status("Funds were deposit", msg.sender, block.timestamp);
    }
    
    function stillAlive() onlyOwner {
        lastTouch = block.timestamp;
        Status("I still alive motherfuckers", msg.sender, block.timestamp);
    }
    
    function isDead() {
        Status("are you dead?", msg.sender, block.timestamp);
        if(block.timestamp > (lastTouch + 120)) {
            giveMoneyToChilds();
        } else {
            Status("I am not dead bastard", msg.sender, block.timestamp);
        }
    }
    
    function giveMoneyToChilds() {
        Status("I am dead, take my money", msg.sender, block.timestamp);
        uint amountPerChild = this.balance/childs.length;
        for (uint i; i < childs.length; i++) {
            childs[i].transfer(amountPerChild);
        }
    }
    
    function addChild(address childAddress) onlyOwner {
        Status("New child added", childAddress, block.timestamp);
        childs.push(childAddress);
    }
    
    
}